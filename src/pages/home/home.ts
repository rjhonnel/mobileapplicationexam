import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController  } from 'ionic-angular';

import { ListPage } from '../list/list';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('signupSlider') signupSlider: any;

  authForm: FormGroup;
  submitAttempt: boolean = false;
  constructor(public navCtrl: NavController, public formBuilder: FormBuilder, public toastCtrl: ToastController) {

  this.authForm = formBuilder.group({
      emailAddress: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('^[^\s@]+@[^\s@]+\.[^\s@]+$'), Validators.required])],
      passwd: ['', Validators.compose([Validators.minLength(4), Validators.required])]
  });

  }

  errorToast(position:string) {
    let toast = this.toastCtrl.create({
      message: 'Please fill out all details accurately.',
      duration: 3000,
      position: position
    });
    toast.present(toast);
  }

  save(){

      if(!this.authForm.valid){
      this.submitAttempt = true;
          this.errorToast('top');
      }
      else {
      this.submitAttempt = false;
          console.log("success!");
          this.navCtrl.setRoot(ListPage, {}, {animate: true, direction: 'forward'});
      }

  }

}
