import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController } from 'ionic-angular';

import { ItemDetailsPage } from '../item-details/item-details';
import { AddProjectModal } from '../modals/add-project-modal';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  items: Array<{id: number, name: string, date_created: string, description: any}>;
  constructor(public navCtrl: NavController, public storage: Storage, public modalCtrl: ModalController) {

    this.storage.get('data').then((val) => {
        if(val !== null){
          this.items = val;
          console.log(val);
        }
    });
  }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }

  openModal() {
    let modal = this.modalCtrl.create(AddProjectModal);
    modal.present();
  }
}


