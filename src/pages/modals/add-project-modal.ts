import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {NavController, ViewController, ToastController} from 'ionic-angular';

import { ListPage } from '../list/list';
@Component({
  templateUrl: 'add-project-modal.html'
})

export class AddProjectModal {
  items: Array<{id: number, name: string, date_created: string, description: any}>;
  addPjctForm: FormGroup;
  date_created: any;
  submitAttempt: boolean = false;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public storage: Storage,
    public toastCtrl: ToastController
  ) {
      this.addPjctForm = formBuilder.group({
      projectName: ['', Validators.compose([Validators.required])],
      projectDescription: ['', Validators.compose([Validators.required])]
    });
  }

  save(){
      //this.storage.clear();
      this.date_created = new Date().toJSON().slice(0,10).replace(/-/g,'/');

      if(!this.addPjctForm.valid){
      this.submitAttempt = true;
      }
      else {
        this.submitAttempt = false;
           this.storage.get('data').then((val) => {
            if(val !== null){
                val.push({id: parseInt(val.slice(-1)[0].id)+1,name:this.addPjctForm.value.projectName,date_created:this.date_created,description:this.addPjctForm.value.projectDescription});
                this.storage.set('data',val);
                this.items = val;
                this.msgToast('top');
            }
            else{
                this.items = [];
                this.items.push({id:1,name:this.addPjctForm.value.projectName,date_created:this.date_created,description:this.addPjctForm.value.projectDescription});
                this.storage.set('data',this.items);
                this.msgToast('top');
            }
        });
      }
  }

  msgToast(position:string) {
    let toast = this.toastCtrl.create({
      message: 'Project was added successfully.',
      duration: 3000,
      position: position
    });
    toast.present(toast);
    this.navCtrl.setRoot(ListPage, {}, {animate: true, direction: 'forward'});
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
